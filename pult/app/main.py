import os
import sys
import time
from datetime import datetime

# from json import dumps
from typing import Annotated, Union

# from urllib.parse import parse_qs
import can
import uvicorn
from cans import cans
from cantpy import cant
from database import *
from database import Base, CANframe, engine
from fastapi import Depends, FastAPI, Form, Request, UploadFile
from fastapi.encoders import jsonable_encoder
from fastapi.responses import FileResponse, HTMLResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.sql import desc, text

print("Start PULT!")
print("file=", __file__)
print("CWD=", os.getcwd())

DIRNAME = os.path.dirname(os.path.abspath(__file__))
print("DIR=", DIRNAME)

DIRAPP = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print("DIRAPP=", DIRAPP)


CAN_PROTOCOL_FILENAME = DIRNAME + "/cantid"

CAN_DEVICE_NAME = "vcan0"

CAN_CONFIG_FILENAME = DIRNAME + "/canconfpult"

if len(sys.argv) == 2:
    CAN_DEVICE_NAME = sys.argv[1]
    print("CAN_Device_name set from file arg = ", CAN_DEVICE_NAME)


print("STEP #1")


class RespFrame(CANframe):
    frm = cant(CAN_PROTOCOL_FILENAME)

    @hybrid_property
    def LoadId(self):
        # return self.dat
        print("INIT#")

        # self.frm.InitId()
        return (
            str(self.frm.getLoadId())
            + ":"
            + str(self.frm.getIdAttrSize())
            + ":"
            + self.dat
        )

    @hybrid_property
    def AttrSize(self):
        s = str(self.frm.getIdAttrSize()) + self.dat - self.dat
        return s

    @hybrid_property
    def G02A(self):
        print("aaa=", self.adr)
        aaa = self.adr
        print("aaa1=", aaa)
        self.frm.setAdr(aaa)
        self.frm.decodeId()
        c = self.frm.getAttrByNdx(2)
        print("attr=", c)
        return c


# создаем таблицы базы данных CANфреймов
Base.metadata.create_all(bind=engine)


# создаем сессию подключения к бд
SessionLocal = sessionmaker(autoflush=False, bind=engine)


# определяем зависимость
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


print("STEP #2")

# название приложения
app = FastAPI()
cant_client = cant(CAN_PROTOCOL_FILENAME)
print("Id size()=", cant_client.getIdAttrSize())
cans_obj = cans()
cans_recv = cans()  # can for Rx

print("STEP #3")
cnt = cant(CAN_PROTOCOL_FILENAME)
print("Id 14 size()=", cant_client.getIdAttrSize())
cnt.LoadIdFile(CAN_PROTOCOL_FILENAME)
print("Id 15 size()=", cant_client.getIdAttrSize())

print("STEP #4")
# указываем директорию, где будут лежать шаблоны страниц, она должна лежать на одном
# уровне с папкой app
templates = Jinja2Templates(directory=DIRAPP + "/templates")

app.mount("/static", StaticFiles(directory=DIRAPP + "/static", html=True))

# StaticFiles(directory=None, packages=None, html=False, check_dir=True)

# globalism
recv_canfrm_started = False

field_names = []
field_lengths = []
field_keys = []


print("STEP #5")


def ContentBuild():
    T1 = '<h3>ОТПРАВКА ФРЕЙМА CAN: </h3> <form action="/pult" method="post">'
    b = 0
    T1 += "<h4> Поле адреса (Id)</h4>"
    for a in field_names:
        m = str(cant_client.getIdAttrMaxValByNdx(b) - 1)

        if cant_client.getIdAttrMaxValByNdx(b) == 2:
            T1 += '<label for="' + a + '">' + a + " :</label><br>"
            for j in range(0, 2):
                T1 += (
                    '<input type="radio" name="'
                    + a
                    + '" value="'
                    + str(j)
                    + ' ">'
                    + '<label for=" '
                    + str(j)
                    + ' ">'
                    + str(j)
                    + "</label><br>"
                )

            # <input type="radio" id="html" name="fav_language" value="HTML">
            # <label for="html">HTML</label><br>
        else:
            T1 += '<label for="' + a + '">' + a + " [0..." + m + "]:</label><br>"
            T1 += (
                '<input type="number" name="'
                + a
                + '" value="'
                + str(0)
                + '" min=0 max='
                + m
                + ' required label="'
                + a
                + '" /><br>'
            )
        b += 1

    # DATA
    T1 += "<h4> Поле данных. Максимум 8 символов. </h4>"
    a = "DATA"
    T1 += '<label for="' + a + '">' + a + ":</label><br>"
    T1 += (
        '<input type="text" maxlength=8 size=8 name="'
        + a
        + '" value="" required label="'
        + a
        + '" /><br>'
    )
    # DATA

    T1 += '<input type="submit" >'
    T1 += "</form>"

    # print(T1)

    html_content_B = """
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8" />
          <title>SEND CAN FRAME</title>

          <link rel="stylesheet" href="http://127.0.0.1:8000/static/pultstyle.css">

      </head>


      <body>
    """

    html_content_E = """

            <p><a id="NAV_back" href="../static">Назад</a></p>
        </body>
    </html>
    """

    html_content = html_content_B + T1 + html_content_E

    return html_content


# def ContentBuild():


def ContentBuildSubSet():
    T1 = ""
    T1 = "<h3>ЗАГРУЗКА ПРАВИЛ ПОДПИСКИ ИЗ ФАЙЛА: </h3>"
    T1 += "<h4> Имеющиеся подписки:</h4>"
    T1 += "<ul>"
    for j in range(cans_obj.SubS_count):
        T1 += "<li>" + str(j) + " : " + " ".join(map(str, cans_obj.SubS[j])) + "</li>"
    T1 += "</ul><br>"
    # FORM
    T1 += '<form action="/subset" enctype="multipart/form-data" method="post">'

    a = "Subfile"

    T1 += '<label for="' + a + '">' + "Новый файл подписки" + " </label><br>"
    T1 += '<input type="file" name="' + a + '" required label="' + a + '" /><br>'
    T1 += "<br>"
    T1 += '<input type="submit" >'

    T1 += "</form>"

    html_content_B = """
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8" />
          <title>Sub: File setting</title>

          <link rel="stylesheet" href="http://127.0.0.1:8000/static/pultstyle.css">

      </head>


      <body>
    """

    html_content_E = """

            <p><a id="NAV_back" href="../static">Назад</a></p>
        </body>
    </html>
    """

    html_content = html_content_B + T1 + html_content_E

    return html_content


# def ContentBuildSubSet():


def ContentBuildSubFile():
    T1 = "<h3>ОТПРАВКА ФАЙЛА ПО ПОДПИСКЕ: "
    T1 = '</h3> <form action="/subfile" enctype="multipart/form-data" method="post">'
    b = "Sub"
    T1 += "<h4> Имеющиеся подписки:</h4>"
    T1 += (
        '<label for="' + b + '">' + "Выберите подписку для пересылки" + " :</label><br>"
    )
    for j in range(cans_obj.SubS_count):
        T1 += (
            '<input type="radio" name="'
            + b
            + '" value="'
            + str(j)
            + ' ">'
            + '<label for=" '
            + str(j)
            + ' ">'
            + " ".join(map(str, cans_obj.SubS[j][4:]))
            + "</label><br>"
        )
    T1 += "<br>"
    a = "Subfile"

    T1 += '<label for="' + a + '">' + "Выберите файл для отправки" + " </label><br>"
    T1 += '<input type="file" name="' + a + '" required label="' + a + '" /><br>'
    T1 += "<br>"

    # NAME
    T1 += "<h4> Имя файла в месте назначения. Максимум 7 символов. </h4>"
    a = "FN"
    T1 += '<label for="' + a + '">' + a + ":</label><br>"
    T1 += (
        '<input type="text" maxlength=7 size=7 name="'
        + a
        + '" value="" required label="'
        + a
        + '" /><br>'
    )
    # NAME

    # FileID

    a = "FileID"
    m = "255"
    T1 += (
        '<label for="'
        + a
        + '">'
        + "Идентификатор файла"
        + " [1..."
        + m
        + "]:</label><br>"
    )
    T1 += (
        '<input type="number" name="'
        + a
        + '" value="'
        + str(1)
        + '" min=1 max='
        + m
        + ' required label="'
        + a
        + '" /><br>'
    )

    # FileID

    T1 += '<input type="submit" >'
    T1 += "</form>"

    html_content_B = """
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8" />
          <title>Sub: Send command</title>

          <link rel="stylesheet" href="http://127.0.0.1:8000/static/pultstyle.css">

      </head>


      <body>
    """

    html_content_E = """

            <p><a id="NAV_back" href="../static">Назад</a></p>
        </body>
    </html>
    """

    html_content = html_content_B + T1 + html_content_E

    return html_content


# def ContentBuildSubFile():


def ContentBuildSubCmd():
    T1 = '<h3>ОТПРАВКА КОМАНДЫ ПО ПОДПИСКЕ: </h3> <form action="/subcmd" method="post">'
    b = "Sub"
    T1 += "<h4> Имеющиеся подписки:</h4>"
    T1 += (
        '<label for="' + b + '">' + "Выберите подписку для пересылки" + " :</label><br>"
    )
    for j in range(cans_obj.SubS_count):
        T1 += (
            '<input type="radio" name="'
            + b
            + '" value="'
            + str(j)
            + ' ">'
            + '<label for=" '
            + str(j)
            + ' ">'
            + " ".join(map(str, cans_obj.SubS[j][4:]))
            + "</label><br>"
        )
    T1 += "<br>"
    a = "CmdCode"
    m = "256"
    T1 += '<label for="' + a + '">' + "Код команды" + " [0..." + m + "]:</label><br>"
    T1 += (
        '<input type="number" name="'
        + a
        + '" value="'
        + str(0)
        + '" min=0 max='
        + m
        + ' required label="'
        + a
        + '" /><br>'
    )
    # DATA
    T1 += "<h4> Поле данных. Максимум 8 символов. </h4>"
    a = "DATA"
    T1 += '<label for="' + a + '">' + a + ":</label><br>"
    T1 += (
        '<input type="text" maxlength=8 size=8 name="'
        + a
        + '" value="" required label="'
        + a
        + '" /><br>'
    )
    # DATA

    T1 += '<input type="submit" >'
    T1 += "</form>"

    # print(T1)

    html_content_B = """
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8" />
          <title>Sub: Send command</title>

          <link rel="stylesheet" href="http://127.0.0.1:8000/static/pultstyle.css">

      </head>


      <body>

    """

    html_content_E = """
        <body>
            <p><a id="NAV_back" href="../static">Назад</a></p>
        </body>
    </html>
    """

    html_content = html_content_B + T1 + html_content_E

    return html_content


# def ContentBuildSubCmd():

print("STEP #6")
# globalism


@app.on_event("startup")
def startup():
    print("\n STRTUP \n")
    with open(CAN_PROTOCOL_FILENAME) as protocol_file:
        lines = protocol_file.readlines()

    print("LINES")
    print(lines)
    print("LINES OK")

    field_names1, field_lengths1, field_keys1 = zip(*(line.split() for line in lines))
    global field_names
    field_names = field_names1
    global field_lengths
    field_lengths = field_lengths1
    global field_keys
    field_keys = field_keys1

    print(field_names1, field_lengths1, field_keys1)
    print(field_names, field_lengths, field_keys)

    cant_client.SetId(field_names, field_lengths, field_keys)

    cans_obj.loadSub(CAN_CONFIG_FILENAME)

    print("\n STRTUP OK \n")


@app.get("/")
def read_root():
    return RedirectResponse("static")


@app.get("/hlo/", response_class=HTMLResponse)
# ВНИМАНИЕ должен быть выше импортирован Request из FastAPI
async def home(request: Request):
    data = {"page": "Home page"}
    return templates.TemplateResponse("page.html", {"request": request, "data": data})


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}


@app.get("/page/{page_name}")
async def page(request: Request, page_name: str):
    data = {"page": page_name}
    txt = "das is einfache text"
    return templates.TemplateResponse(
        "page.html", {"request": request, "data": data, "txt": txt}
    )


"""
здесь request - это собственно запрос страницы

"data": data - пара ключ/значение, где ключ - имя переменной в шаблоне, а значение имя
переменной в питоне

"""


@app.get("/twoforms", response_class=HTMLResponse)
def form_get(request: Request):
    result = "Задайте в полях число"
    return templates.TemplateResponse(
        "twoform.html", context={"request": request, "result": result}
    )


@app.post("/form1", response_class=HTMLResponse)
def form_post1(request: Request, number: int = Form(...)):
    result = number + 2
    return templates.TemplateResponse(
        "twoform.html",
        context={"request": request, "result": result, "yournum": number},
    )


@app.post("/form2", response_class=HTMLResponse)
def form_post2(request: Request, number: int = Form(...), action: str = Form(...)):
    # print('post_form2')
    if action == "a100":
        result = number + 100
    elif action == "m100":
        result = number * 100
    return templates.TemplateResponse(
        "twoform.html",
        context={"request": request, "result": result, "yournum": number},
    )


@app.get("/pult")
def pult_get():
    return HTMLResponse(content=ContentBuild(), status_code=200)


@app.post("/pult")
async def check(request: Request):
    print("Start PULT post")
    da = await request.form()
    da = jsonable_encoder(da)
    # print(da)
    lda = len(da)
    print("da length=", lda)
    for i in range(0, lda - 2):
        print("i=", i, " , ", field_names[i], " : ", da[field_names[i]])

    print("Start CAN")

    print("SetID")
    print("field", field_names, field_lengths, field_keys)

    print("setID ok")
    IdSz = cant_client.getIdAttrSize()
    print("IDattrSize=", IdSz, "lda=", lda)
    # <<<
    for i in range(0, IdSz):
        print("i=", i, " , ", field_names[i], "=", da[field_names[i]])
        print(
            "Size=",
            field_lengths[i],
            " Max value=",
            cant_client.getIdAttrMaxValByNdx(i),
        )
        nu = int(da[field_names[i]])
        t = cant_client.setAttrByName(bytes(field_names[i], "ascii"), nu)
        print(
            "setAttrByName[",
            i,
            "]=",
            t,
            ">>",
            nu,
            ">>",
            cant_client.getAttrByName(bytes(field_names[i], "ascii")),
        )
        if not (t):
            t = cant_client.setAttrByName(bytes(field_names[i], "ascii"), 0)
            print("2. setAttrByNdx[", i, "]=", t)

    for i in range(0, IdSz):
        print(
            "i=",
            i,
            "atrName=",
            field_names[i],
            "can=",
            cant_client.getAttrByName(bytes(field_names[i], "ascii")),
        )

    print("codeId()      ", cant_client.codeId())

    cans_obj.start(CAN_DEVICE_NAME)

    print("Start Send Post Pult")

    DT = str(da["DATA"])

    for i in range(1):
        time.sleep(1)
        print("step=", i)
        print("getAdr()    ", cant_client.getAdr())

        cans_obj.sendFrameIdData(cant_client.getAdr(), DT)

    cans_obj.stop()

    print("Finish!")

    return HTMLResponse(content=ContentBuild(), status_code=200)


@app.get("/subcmd")
def subcmd_get():
    return HTMLResponse(content=ContentBuildSubCmd(), status_code=200)


@app.post("/subcmd")
async def subcmd_post(request: Request):
    print("Start subcmd post")
    da = await request.form()
    da = jsonable_encoder(da)
    # print(da)
    lda = len(da)
    print("da length=", lda)
    print("sub=", da["Sub"])
    print("CmdCode=", da["CmdCode"])
    print("DATA=", da["DATA"])

    print("Start CAN")

    print("SetID")
    print("field", field_names, field_lengths, field_keys)

    print("setID ok")
    IdSz = cant_client.getIdAttrSize()
    print("IDattrSize=", IdSz, "lda=", lda)

    for i in range(0, IdSz):
        cant_client.setAttrByNdx(i, 0)

    cant_client.setAttrByName(bytes("ID_CRC8", "ascii"), int(da["CmdCode"]))
    print("SET DATA subcmd!")
    cant_client.setData(da["DATA"])

    # cant_client.setAttrByName(bytes(field_names[i], "ascii"), nu)

    for i in range(0, IdSz):
        print(
            "i=",
            i,
            "atrName=",
            field_names[i],
            "can=",
            cant_client.getAttrByName(bytes(field_names[i], "ascii")),
        )

    print("codeId()      ", cant_client.codeId())

    print("Device CAN=", cans_obj.name)

    cans_obj.start(cans_obj.name)

    print("Start Send Post Subcmd")

    # DT = str(da["DATA"])
    # print("DATA=", DT)

    cans_obj.sendFrameSub(cant_client, int(da["Sub"]), True)

    cans_obj.stop()

    print("Finish!")

    return HTMLResponse(content=ContentBuildSubCmd(), status_code=200)


# async def subcmd_post(request: Request):


@app.get("/subset")
def subset_get():
    return HTMLResponse(content=ContentBuildSubSet(), status_code=200)


@app.post("/subset")
async def subset_post(Subfile: UploadFile):
    print("Start subset post")
    print("file name:", Subfile.filename)
    lines = Subfile.file.readlines()
    print(lines)
    cans_obj.initSub(lines)
    return HTMLResponse(content=ContentBuildSubSet(), status_code=200)


@app.get("/subfile")
def subfile_get():
    return HTMLResponse(content=ContentBuildSubFile(), status_code=200)


@app.post("/subfile")
async def subfile_post(
    Sub: Annotated[str, Form()],
    FN: Annotated[str, Form()],
    FileID: Annotated[str, Form()],
    Subfile: UploadFile,
):
    print("Start subfile post")
    print("Sub:", Sub)
    print("FN:", FN)
    print("FileID:", FileID)

    print("file name:", Subfile.filename)

    filebody = Subfile.file.read()
    print(filebody, len(filebody))

    # Send file
    datagramLength = 4
    FileLength = len(filebody)

    FrameCount = int(FileLength / datagramLength)
    FinalDatagramLen = (FileLength) % datagramLength

    print("FrameCount=", FrameCount, "FinalDatagramLen=", FinalDatagramLen)

    IdSz = cant_client.getIdAttrSize()
    print("IDattrSize=", IdSz)

    for i in range(0, IdSz):
        cant_client.setAttrByNdx(i, 0)

    cant_client.setAttrByName(bytes("Type", "ascii"), 1)

    # cant_client.setAttrByName(bytes("ID_CRC8", "ascii"), int(da["CmdCode"]))
    # cant_client.setData(da["DATA"])

    for i in range(0, IdSz):
        print(
            "i=",
            i,
            "atrName=",
            field_names[i],
            "can=",
            cant_client.getAttrByName(bytes(field_names[i], "ascii")),
        )

    print("codeId()      ", cant_client.codeId())
    # Start CAN device
    print("Device CAN=", cans_obj.name)
    cans_obj.start(cans_obj.name)
    #

    # заголовок
    cant_client.setAttrByName(bytes("ID_CRC8", "ascii"), 0)
    print("FN=", FN)
    FID = int(FileID)
    # cant_client.setData(chr(int(FileID)) + FN)

    cant_client.codeId()
    x = [FID.to_bytes(1, byteorder="little"), bytes(FN, "ascii")]
    cant_client.Data = b"".join(x)

    print("cant_client.Data", cant_client.Data)
    print("Head \n")
    cans_obj.sendFrameSub(cant_client, int(Sub), True)

    # длина
    cant_client.setAttrByName(bytes("ID_CRC8", "ascii"), int(FileID))
    x = [
        (0).to_bytes(4, byteorder="little"),
        FileLength.to_bytes(4, byteorder="little"),
    ]
    cant_client.Data = b"".join(x)

    print("cant_client.Data", cant_client.Data)

    cant_client.codeId()

    print("Length \n")
    cans_obj.sendFrameSub(cant_client, int(Sub), True)

    # тело
    print("Body \n")
    for pos in range(FrameCount):
        bg = pos * datagramLength
        en = bg + datagramLength
        # print(pos, " : ", filebody[bg:en])
        x = [
            (pos + 1).to_bytes(4, byteorder="little"),
            filebody[bg:en],
        ]
        print(x)
        cant_client.Data = b"".join(x)
        cans_obj.sendFrameSub(cant_client, int(Sub), True)

    # заключительный кадр
    print("Tail \n")
    if FinalDatagramLen > 0:
        bg = FileLength - FinalDatagramLen
        x = [
            (FrameCount + 1).to_bytes(4, byteorder="little"),
            filebody[bg:],
        ]
        print(x)
        cant_client.Data = b"".join(x)
        cans_obj.sendFrameSub(cant_client, int(Sub), True)

    # Stop CAN device
    cans_obj.stop()

    print("Finish!")

    # Send file

    return HTMLResponse(content=ContentBuildSubFile(), status_code=200)


# async def subfile_post(


def canmsgprint(msg: can.Message) -> None:
    # print(
    #     "msg=",
    #     msg.arbitration_id,
    #     " : ",
    #     msg.data,
    #     " : ",
    #     msg.timestamp,
    #     " : ",
    #     msg.channel,
    # )
    print("CAN fame START stored in Database!!!")
    db = SessionLocal()

    print("aaa=", msg.arbitration_id)
    cnt.setAdr(msg.arbitration_id)
    cnt.decodeId()
    # for ndx in range(cnt.getIdAttrSize()):
    #     c = cnt.getAttrByNdx(ndx)
    #     print("attr=", c)
    # print("data len=", len(msg.data))
    LL = len(msg.data)
    frm = CANframe(adr=msg.arbitration_id)
    if LL > 0:
        frm.d0 = msg.data[0]
    if LL > 1:
        frm.d1 = msg.data[1]
    if LL > 2:
        frm.d2 = msg.data[2]
    if LL > 3:
        frm.d3 = msg.data[3]
    if LL > 4:
        frm.d4 = msg.data[4]
    if LL > 5:
        frm.d5 = msg.data[5]
    if LL > 6:
        frm.d6 = msg.data[6]
    if LL > 7:
        frm.d7 = msg.data[7]

    print("timestamp,", msg.timestamp)
    frm.tim = datetime.fromtimestamp(msg.timestamp)
    print("tim,", frm.tim)
    frm.a0 = cnt.getAttrByNdx(0)
    frm.a1 = cnt.getAttrByNdx(1)
    frm.a2 = cnt.getAttrByNdx(2)
    frm.a3 = cnt.getAttrByNdx(3)
    frm.a4 = cnt.getAttrByNdx(4)
    frm.a5 = cnt.getAttrByNdx(5)
    frm.a6 = cnt.getAttrByNdx(6)
    frm.a7 = cnt.getAttrByNdx(7)

    db.add(frm)
    db.commit()
    db.refresh(frm)
    # tframe = CANframe(adr=2067478, dat=b"BBBBBB", tim=1699266191.8744552)
    # db.add(tframe)  # добавляем в бд
    # db.commit()  # сохраняем изменения
    # db.refresh(tframe)
    print("CAN fame stored in Database!!!")


@app.get("/canrcv")
async def read_can():
    print("start read_can")
    global recv_canfrm_started
    if not recv_canfrm_started:
        cans_recv.start(CAN_DEVICE_NAME)
        # msg = cans_obj.bus.recv()
        # for msg in cans_obj.bus:
        #    print(f"{msg.arbitration_id:X}: {msg.data}")

        # print("msg=", msg)
        global notifier

        notifier = can.Notifier(
            cans_recv.bus, [can.Logger("recorded.log"), canmsgprint]
        )
        recv_canfrm_started = True
        return {"Процесс стартовал."}
    else:
        return {"Процесс уже был запущен!"}


@app.get("/canstop")
async def stop_can():
    print("stop read_can")
    global recv_canfrm_started
    if recv_canfrm_started:
        global notifier
        notifier.stop()
        cans_recv.stop()
        recv_canfrm_started = False
        return {"Процесс остановлен."}
    else:
        return {"Процесс не был запущен!"}


# async def stop_can():


@app.get("/rcv_can_process")
async def get_rcv_can_process():
    # print("get_rcv_can_process")
    global recv_canfrm_started
    if recv_canfrm_started:
        return {"Процесс идет..."}
    else:
        return {"Процесс остановлен."}


# async def get_rcv_can_proces():


@app.get("/dbq1")
def get_allframes3(db: Session = Depends(get_db)):
    print("Start dbq1")

    qq = (
        db.query(
            # CANframe.tim,
            # CANframe.dat,
            # CANframe.a2,
            # CANframe.a3,
            # CANframe.a4,
            # CANframe.a5,
            # CANframe.a6,
            CANframe
        )
        .filter(CANframe.a4 == 11)
        .all()
    )
    # print("qq", qq)
    # jsn = jsonable_encoder(qq)  # dumps(parse_qs(qq))
    # print("jsn", jsn)
    # print("jsn OK")
    # for l in qq:
    #     print(l)

    # return HTMLResponse(content=jsn, status_code=200)
    return qq


#     return db.query(CANframe).filter(CANframe.adr > 2069000).all()


@app.get("/dbadd")
def create_person(db: Session = Depends(get_db)):
    tframe = CANframe(adr=2067478, tim=datetime.fromtimestamp(1699266191.8744552))
    tframe.d0 = 65
    tframe.d1 = 65
    tframe.d2 = 65
    tframe.d3 = 65
    tframe.d4 = 65
    tframe.d5 = 65
    tframe.d6 = 65
    tframe.d7 = 65

    db.add(tframe)  # добавляем в бд
    db.commit()  # сохраняем изменения
    db.refresh(tframe)
    return tframe


@app.get("/dbdelall")
def get_deleteframes(db: Session = Depends(get_db)):
    a = db.query(CANframe).delete()
    db.commit()  # сохраняем изменения
    return a


@app.get("/dball", response_class=HTMLResponse)
def get_allframes(request: Request, db: Session = Depends(get_db)):
    return templates.TemplateResponse(
        "Q_allFrames.html",
        {
            "request": request,
            "title": "All frames",
            "body_content": "Все принятые CAN-фреймы",
            "cn": field_names,
            "frames": db.query(CANframe).order_by(CANframe.tim).all(),
        },
    )


@app.get("/dbq2/", response_class=HTMLResponse)
def get_allframes2(FF, RC: int, request: Request, db: Session = Depends(get_db)):
    return templates.TemplateResponse(
        "Q_allFrames.html",
        {
            "request": request,
            "title": "All frames",
            "body_content": "Принятые CAN-фреймы",
            "cn": field_names,
            "frames": db.query(CANframe)
            .filter(text("frames." + FF + " == " + str(RC)))
            # text('CANframe.a4 == 12')
            .order_by(CANframe.tim)
            .all(),
        },
    )


@app.get("/dbq3")
def get_dbq3(FF, RC: int, db: Session = Depends(get_db)):
    # print("Start dbq4", FF, RC)
    qq = db.query(CANframe).filter(text("frames." + FF + " == " + str(RC))).all()

    return qq


@app.get("/dbq4")
def get_dbq4(FF, RC: str, db: Session = Depends(get_db)):
    print("Start dbq4", FF, RC)
    ln = "frames." + FF + str(RC)
    print(ln)
    qq = db.query(CANframe).filter(text(ln)).all()
    # qq = db.query(CANframe).filter(CANframe.d0.in_([1, 2, 3])).all()

    return qq


@app.get("/dbq5")
def get_dbq5(FF: str, db: Session = Depends(get_db)):
    print("Start dbq5", FF)
    qq = db.query(CANframe).filter(text(FF)).order_by(desc(CANframe.tim)).all()
    # qq = db.query(CANframe).filter(CANframe.d0.in_([1, 2, 3])).all()

    return qq


# запись файла отчета
@app.put("/Reportsave")
def get_reportesave(FF: str, FNM: str, db: Session = Depends(get_db)):
    print("Start reportsave", FNM, FF)
    qq = db.query(CANframe).filter(text(FF)).all()

    print("file ready to save to ", DIRAPP + "/" + FNM)
    import csv

    outfile = open(DIRAPP + "/reports/" + FNM + ".csv", "w")
    outcsv = csv.writer(outfile)

    # for column in CANframe.__mapper__.columns:
    #    print("AAA=>", str(column.name))

    outcsv.writerow([column.name for column in CANframe.__mapper__.columns])

    [
        outcsv.writerow(
            [getattr(curr, column.name) for column in CANframe.__mapper__.columns]
        )
        for curr in qq
    ]

    outfile.close()
    # return qq


# фильтрация CAN фреймов по одному полю
@app.get("/qqqq")
def get_qqqq():
    return FileResponse(DIRAPP + "/static/query.html")


@app.get("/T0", response_class=HTMLResponse)
def get_T0(request: Request):
    return templates.TemplateResponse(
        "Q_allFrames.html",
        {
            "request": request,
            "title": "T0 Demo",
            "body_content": "This is the demo for using FastAPI with Jinja templates",
            "cn": field_names,
        },
    )


# ПРИЕМ ФАЙЛОВ
@app.get("/frx")
def get_frx():
    return FileResponse(DIRAPP + "/static/filerx.html")


# заголовки файлов
@app.get("/fileheads")
def get_fileheads(db: Session = Depends(get_db)):
    # print("Start dbq4", FF, RC)
    qq = db.query(CANframe).filter(CANframe.a2 == 0, CANframe.a7 == 1).all()

    return qq


# тело файла
@app.get("/filebody")
def get_filebody(FNM, IDF: int, db: Session = Depends(get_db)):
    print("Start filebody", FNM, IDF)
    qq = db.query(CANframe).filter(CANframe.a7 == 1, CANframe.a2 == IDF).all()

    return qq


# запись файла
@app.put("/filesave")
def get_filesave(FNM, IDF: int, db: Session = Depends(get_db)):
    print("Start filebody", FNM, IDF)
    qq = db.query(CANframe).filter(CANframe.a7 == 1, CANframe.a2 == IDF).all()
    LL = qq[0].d4
    print("FileLength=", LL)
    # M = []
    S = ""
    for q in qq:
        if q.d0 > 0:
            S += chr(q.d4) + chr(q.d5) + chr(q.d6) + chr(q.d7)
    S = S[:LL]
    print(S)
    print("file ready to save to ", DIRAPP + "/" + FNM)
    file = open(DIRAPP + "/" + FNM, "w")
    file.write(S)
    file.close()


# @app.put("/filesave")


# РАБОТА С КОМАНДАМИ
@app.get("/sublist")
def get_sublist():
    print("Start get_sublist!")
    qb = []
    for j in range(cans_obj.SubS_count):
        qb.append(
            {
                "typ": "Прием",
                "a3": cans_obj.SubS[j][0],
                "a4": cans_obj.SubS[j][1],
                "a5": cans_obj.SubS[j][2],
                "a6": cans_obj.SubS[j][3],
            }
        )
    for j in range(cans_obj.SubS_count):
        qb.append(
            {
                "typ": "Передача",
                "a3": cans_obj.SubS[j][4],
                "a4": cans_obj.SubS[j][5],
                "a5": cans_obj.SubS[j][6],
                "a6": cans_obj.SubS[j][7],
            }
        )

    return qb


@app.get("/getcandev")
async def get_candevice():
    print("get device_can")
    global CAN_DEVICE_NAME
    return {CAN_DEVICE_NAME}


# async def get_candevice():


# запись файла отчета
@app.put("/CANdevicesave")
def set_candevice(DNM: str):
    print("Start CANdevicesave", DNM)
    global CAN_DEVICE_NAME
    CAN_DEVICE_NAME = DNM


# def set_candevice(DNM: str):


# Application START!!!
if __name__ == "__main__":
    uvicorn.run(app=app, host="0.0.0.0", port=8000)
