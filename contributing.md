# Рекомендации по внесению изменений

## 1. Workflow (использование fork и feature branch)

- Сделать
  [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#create-a-fork)
  репозитория.
- Закрепить за собой задачу через добавление комментария в задаче (issue), что эту
  задачу будете делать именно вы. Назначить задачу на себя не получится (изменить поле
  assignee задачи), т.к. для этого нужны права, например, роли developer, для
  репозитория. При работе через создание собственного fork получение прав не требуются.
- В своём fork сделать ветку
  [feature/36-short_description](https://docs.gitlab.com/ee/user/project/repository/branches/#prefix-branch-names-with-issue-numbers),
  где вместо 36 указать номер
  [issue](https://gitlab.com/Zaynulla/aerospace_computing_systems/-/issues), над которым
  работаете. Если соответствующей задачи нет, то можете указать любое осмысленное
  название вида feature/short_change_description. Ветку необходимо создавать из ветки
  devel.
- Сделать коммиты в новой ветке в своём форке репозитория.
- Убедиться, что пройдены все
  [проверки](https://stepik.org/lesson/1099950/step/9?unit=1110938), при необходимости
  поправить в новых коммитах.
- Создать [merge
  request](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merge-changes-back-upstream)
  на вливание ветки из Вашего fork в ветку devel исходного репозитория. В
  [комментарии](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
  на запрос слияния (merge request), указать

```text
Closes #36
где вместо 36 поставить номер своего issue
можно добавить и другой текст. Так после подтверждения merge request
(запрос на вливание) код окажется в основной ветке, а issue (задача) будет закрыта
автоматически.
```

## 2. Использование автоматических инструментов IDE

При работе в VSCode многие проверки адаптированы под проект и сохранены в
соответствующих файлах конфигурации. В том числе при каждом сохранении файла выполняются
некоторые автоматические исправления (форматирование, удаление неиспользуемых импортов и
т.д.). См. инструкции по [распознаванию настроек
VSCode](https://stepik.org/lesson/1099950/step/2?unit=1110938) и по [установке
необходимых расширений в VSCode](https://stepik.org/lesson/1099950/step/3?unit=1110938).

## 3. Принятые для проекта стандарты кода

- Все языки программирования: см. файл
  [docs/guidelines/all_languages.md](docs/guidelines/all_languages.md)
- Python: см. файл [docs/guidelines/python.md](docs/guidelines/python.md)
- C++: см. файл [docs/guidelines/cpp.md](docs/guidelines/cpp.md)
- Документация в формате markdown: см. файл
  [docs/guidelines/markdown.md](docs/guidelines/markdown.md)
