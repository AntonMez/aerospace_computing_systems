#include "cant.h"
#include <fstream>

#define CAN_ID_FILE_NAME "cantid"

// cant::cant() : IdAttrName({"Flags", "Attribute", "Receiver", "Sender"}),
// IdAttrLength({5, 8, 8, 8})
cant::cant(std::vector<std::string> name, std::vector<uint8_t> length,std::vector<bool> key)
    : IdAttrName(name), IdAttrLength(length),IdAttrKey(key)
{
    LoadId = false;

    std::ifstream in(CAN_ID_FILE_NAME); // открываем файл для чтения
    if (in.is_open())
        {
            printf("Id Structure Loaded from file! \n");
            LoadId = true;
            std::string NM;
            int LN;
            bool KY;

            IdAttrName.clear();
            IdAttrLength.clear();
            IdAttrKey.clear();

            while (in >> NM >> LN>>KY)
                {
                    //printf("NM=%s LN=%d KY=%d\n", NM.c_str(), LN, KY);
                    IdAttrName.push_back(NM);
                    IdAttrLength.push_back(LN);
                    IdAttrKey.push_back(KY);
                }

            if (IdAttrName.size() != IdAttrLength.size())
                {
                    printf("ERROR cant IdLoad!!! "
                           "IdAttrName.size()!=IdAttrLength.size()");
                    exit(111);
                };
        }
    in.close(); // закрываем файл


    InitId();
    PrintIdStructure();

}; // cant::cant()

void cant::InitId()
{
    //printf("InitId#1\n");
    // building structure

    IdAttrValue.clear();
    IdAttrMask.clear();
    IdAttrShift.clear();
    IdAttrMaxVal.clear();

    uint8_t s = 0;
    uint32_t Uno = 1;
    int N = IdAttrName.size();printf("N =%d \n",N);

    for (int i = 0; i < N; i++)
        s += IdAttrLength[i];

    if (s > IdSize)
        {
            printf("CANT ERROR:(s=%d) > (IdSize=%d) ; Fields count=%d \n",s,IdSize,N);
            PrintIdStructure();
            exit(101);
        }

    // init Values
    //printf("InitId#2\n");

    for (int i = 0; i < N; i++)
        {
            IdAttrValue.push_back(0);
            IdAttrShift.push_back(0);
            IdAttrMask.push_back(Uno);
            uint32_t s = 2;
            for (int j = 1; j < IdAttrLength[i]; j++)
                s *= 2;

            IdAttrMaxVal.push_back(s);
        }

    // building mask
    //printf("InitId#3\n");

    for (int i = 0; i < N; i++)
        {

            for (int k = 0; k < (IdAttrLength[i] - 1); k++)
                {
                    IdAttrMask[i] = IdAttrMask[i] << 1;
                    IdAttrMask[i] |= Uno;

                } // for k
            for (int j = (i + 1); j < N; j++)
                for (int q = 0; q < IdAttrLength[j]; q++)
                    IdAttrShift[i]++;

            IdAttrMask[i] = IdAttrMask[i] << IdAttrShift[i];
        } // for i

    frm.can_dlc = dlc;

    codeId();

    for (int i = 0; i < dlc; i++)
        frm.data[i] = 0;

    //printf("InitId#4\n");

}; // cant::InitId()

void cant::ClearIdStructure()
{
    printf("Start CPP ClearIdStructure!!!");
    IdAttrName.clear();
    IdAttrLength.clear();
    IdAttrKey.clear();
    //PrintIdStructure();

}; // cant::ClearIdStructure

void cant::AddIdStruct(std::string Name, uint8_t Length, bool Key)
{
    IdAttrName.push_back(Name);
    IdAttrLength.push_back(Length);
    IdAttrKey.push_back(Key);
    //PrintIdStructure();

}; // cant::AddIdStruct

//
uint8_t cant::getIdAttrSize()
{
    return IdAttrName.size();
}; // cant::getIdAttrSize

bool cant::getLoadId() { return LoadId; }; // cant::getLoadId

//
void cant::codeId()
{
    int N = IdAttrName.size();
    uint32_t Id = 0;
    uint32_t S;
    for (int i = 0; i < N; i++)
        {
            S = IdAttrValue[i] << IdAttrShift[i];
            Id |= S;
        } // for i

    Id |= CanEFFflg;

    frm.can_id = Id;

}; // cant::codeId()

//
void cant::decodeId()
{
    int N = IdAttrName.size();
    uint32_t Id = frm.can_id;
    uint32_t S;
    for (int i = 0; i < N; i++)
        {
            S = (Id & IdAttrMask[i]);
            IdAttrValue[i] = (uint8_t)(S >> IdAttrShift[i]);
        } // for i

}; // cant::decodeId()

bool cant::setAttrByName(std::string Name, uint8_t Val)
{
    auto it = std::find(IdAttrName.begin(), IdAttrName.end(), Name);

    if (it == IdAttrName.end())
        {
            printf("WRONG IdAttrName!!!");
            exit(102);
            return 0;
        }
    else
        {
            int ndx = it - IdAttrName.begin();

            if (Val > IdAttrMaxVal[ndx])
                {
                    IdAttrValue[ndx] = 0;
                    return 0;
                }
            else
                {
                    IdAttrValue[ndx] = Val;
                    return 1;
                }
        }

}; // cant::setAttrByName(std::string Name,uint8_t Val)

bool cant::setAttrByNdx(int ndx, uint8_t Val)
{
    if ((ndx >= 0) && (ndx < getIdAttrSize()))
        {

            if (Val > IdAttrMaxVal[ndx])
                {
                    IdAttrValue[ndx] = 0;
                    return 0;
                }
            else
                {
                    IdAttrValue[ndx] = Val;
                    return 1;
                }
        }
    else
        {

            return 0;
        }

}; // cant::setAttrByNdx(int ndx, uint8_t Val)

uint32_t cant::getAttrByName(std::string Name)
{

    auto it = std::find(IdAttrName.begin(), IdAttrName.end(), Name);

    if (it == IdAttrName.end())
        {
            printf("WRONG IdAttrName!!!");
            exit(102);
            return 0;
        }
    else
        {
            int ndx = it - IdAttrName.begin();
            return IdAttrValue[ndx];
        }

}; // cant::getAttrByName(std::string Name)

uint32_t cant::getAttrByNdx(int ndx)
{

    if ((ndx >= 0) && (ndx < getIdAttrSize()))
        {
            return IdAttrValue[ndx];
        }
    else
        {
            printf("WRONG Ndx!!!");
            exit(102);
            return 0;
        }

}; // cant::getAttrByNdx(int ndx)

uint32_t cant::getIdAttrMaxValByNdx(int ndx)
{

    if ((ndx >= 0) && (ndx < getIdAttrSize()))
        {
               return IdAttrMaxVal[ndx];
        }
    else
        {
            printf("WRONG Ndx!!!");
            exit(103);
            return 0;
        }

}; // cant::getIdAttrMaxValByNdx(int ndx)


uint8_t cant::getIdAttrKeyByNdx(int ndx)
{
if ((ndx >= 0) && (ndx < getIdAttrSize()))
        {
               return IdAttrKey[ndx];
        }
    else
        {
            printf("getIdAttrKeyByNdx WRONG Ndx!!!");
            exit(103);
            return 0;
        }
}//cant::getIdAttrKeyByNdx(int ndx)


uint32_t cant::getAdr() { return frm.can_id | CanEFFflg; }; // cant::getAdr

void cant::setAdr(uint32_t adr) { frm.can_id=adr; };

uint8_t cant::getKeyCount(){
    uint8_t c=0;
    for(int i =0; i< IdAttrKey.size(); i++)
         if(IdAttrKey[i])
                     c++;
    return c;

};//cant::getKeyCount()

can_frame *cant::getFrame()
{
    codeId();

    return (&frm);

}; //*cant::getFrame

bool cant::input8char(char *C)
{ // char[8]->data
    for (int i = 0; i < dlc; i++)
        frm.data[i] = C[i];
    return true;
}; // cant::input8char


bool cant::inputInt32(uint32_t Val, uint8_t StartPos)
{//32 bit (4 byte) -> data[StartPos]
    union {
	         uint32_t val;
	         uint8_t bytes[4];
           } convert;

    convert.val=Val;

    for(int i=0;i<4;i++)
         frm.data[i+StartPos]=convert.bytes[i];

    return true;
};//cant::inputInt32

uint32_t cant::outputInt32(uint8_t StartPos)
{// data[StartPos] -> 32 bit (4 byte)

     union {
	         uint32_t val;
	         uint8_t bytes[4];
           } convert;

     for(int i=0;i<4;i++)
         convert.bytes[i]=frm.data[i+StartPos];

    return convert.val;

};//cant::outputInt32


uint8_t cant::PrintFrame()
{
    decodeId();
    int N = getIdAttrSize();
    printf("Adr %d ", getAdr());
    for (int i = 0; i < N; i++)
        {
            std::string C = IdAttrName[i];
            int v = (int)getAttrByNdx(i);
            printf("%c:%d ", C[0], v);
        }; // for i

    printf(" | ");
    for (int k = 0; k < frm.can_dlc; k++)
        printf("%d ", frm.data[k]);

    printf("\n");

    return 0;
}; // cant::PrintFrame()


void cant::PrintIdStructure()
{//
    int N = IdAttrName.size();

    for (int i = 0; i < N; i++){

        std::string NM=IdAttrName[i];
        int LN=IdAttrLength[i];
        bool KY=IdAttrKey[i];
        int MV=IdAttrMaxVal[i];

        printf("Field#%d Name=%s Len=%d Key=%d  MaxVal=%d\n", i, NM.c_str(), LN, KY, MV);

    };// for (int i

};//cant::PrintIdStructure()

void cant::test(std::string a)
{
    std::string b;
    b = a;
    // a = "fgh";
    printf("test=%s \n", b.c_str());
}; // void cant::test

cant::~cant(){

}; // cant::~cant()
