#include "cans.h"

#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <iostream>

cans::cans(){

}; // cans::cans()

void cans::start(std::string DevName)
{

    name = DevName;

    // Прежде всего нужно создать сокет. Функция эта имеет три параметра:
    // PF_CAN – domain/protocol family то есть тип протокола (у нас тут can).
    //  SOCK_RAW - type of socket (raw or datagram) – тип сокета,
    //  CAN_RAW – протокол сокета (у нас тут снова can)
    if ((socan = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
        {
            perror("Socket CAN ERROR!!!");
            exit(101);
        }

    fcntl(socan, F_SETFL, O_NONBLOCK); // Неблокирующий сокет

    std::cout << "Socket CAN Ok!\n" << std::endl;

    // Теперь нужно получить индекс интерфейса, который запущен в системе.
    // Обычно эти имена can0, can1, vcan0 .
    //  копируем в Нуль-строку структуры данных имя интерфейса, который у нас
    //  поднят.
    strcpy(ifrcan.ifr_name, name.c_str()); // strcpy(ifrcan.ifr_name, "can0");
    // Если же использовать НОЛЬ в качестве индекса интерфейса,
    //  то будем получать в открытый сокет все, что приходит
    //  на все запущенные интерфейсы can!
    //  Чтобы получить такой индекс надо сделать контрольный звонок (ioctl) в
    //  требуемый интерфейс и сохранить результат в структуре данных struct
    //  ifreq
    ioctl(socan, SIOCGIFINDEX, &ifrcan);
    // Теперь функцией bind связываем сокет и запущенный интерфейс.
    // Сначала создаем и заполняем структуру данных адреса
    memset(&addrcan, 0, sizeof(addrcan));
    addrcan.can_family = AF_CAN;
    addrcan.can_ifindex = ifrcan.ifr_ifindex;

    // Теперь связываем
    std::cout << "Bind CAN " << std::endl;
    if (bind(socan, (struct sockaddr *)&addrcan, sizeof(addrcan)) < 0)
        {
            perror("Bind CAN Error!");
            exit(102);
        }

    std::cout << "Bind CAN " << name << " Ok! socan=" << socan << std::endl;

}; // cans::start(std::string DevName)

void cans::stop()
{

    if (close(socan) < 0)
        { // Закрываем сокет в конце штатной работы
            perror("CAN close Error!!!");
            // return 1;
            exit(400);
        } // if
    std::cout << " socan " << name << " closed!" << std::endl;

}; // cans::stop()

void cans::PrintSub(uint8_t ndx){

        std::cout<<(int)ndx<<". in: ";
        for(int k=0; k<KeyCount;k++)
               std::cout<<SubS[ndx][k]<<" ";

        std::cout<<" out: ";
        for(int k=0; k<KeyCount;k++)
               std::cout<<SubS[ndx][k+KeyCount]<<" ";

        std::cout<<std::endl;

}//cans::PrintSub(uint8_t ndx)


void cans::PrintSubS(){

    std::cout<<"Current Subscribes:"<<std::endl;
    for(int i=0; i<SubScount;i++){
        PrintSub(i);
    };//for i

};//cans::PrintSubS()



int cans::sendFrame(cant F)
{
    bytes = send(socan, F.getFrame(), sizeof(struct can_frame), MSG_DONTWAIT);
    //sleep(1);
    usleep(100);



    if (bytes < 0)
        {
            printf("ERROR CANSEND SERVER!!! \n");
            sleep(1);
        };
    return bytes;

}; // cans::sendFrame(cant F)

int cans::recvFrame(cant *F, int mode, int timeout_sec)
{

    // cant F;
    can_frame CF;

    // Задаем таймаут
    timeval timeout;
    timeout.tv_sec = 10;
    timeout.tv_usec = 10000000; // в миллисекундах

    // Заполняем множество сокетов
    fd_set readset;    // запрос на считывание данных
    FD_ZERO(&readset); // обнуляем
    FD_SET(socan, &readset); // добавляем сокет

    int RezSelect;

    if (timeout_sec <= 0)
        {
            // ждем до упора
            RezSelect = select(socan + 1, &readset, NULL, NULL, NULL);
        }
    else
        {
            // Задаем таймаут
            timeval timeout;
            timeout.tv_sec = timeout_sec;
            timeout.tv_usec = timeout_sec * 1000000; // в миллисекундах
            RezSelect = select(socan + 1, &readset, NULL, NULL, &timeout);
        };

    // if (select(socan + 1, &readset, NULL, NULL, NULL) < 0) //
    // if(select(mx+1, &readset, NULL, NULL, &timeout) < 0)
    if (RezSelect < 0)
        // if(select(mx+1, &readset, &writeset, &errset, &timeout) < 0)
        {
            perror("error select");
            std::cout << "errno=" << errno << std::endl;
            // std::cout<<"timeout="<<timeout.tv_sec<<std::endl;
            if (errno == EWOULDBLOCK)
                std::cout << "errno==EWOULDBLOCK" << std::endl;
            // exit(3);
            return -1;
        }

    if (FD_ISSET(socan, &readset))
        {

            // Поступили данные от CAN. Читаем их во frame
            switch (mode)
                {
                case 0:
                    { // waiting
                        bytes = recv(socan, &CF, sizeof(struct can_frame),
                                     MSG_WAITALL); // recv(*it, buf, 1024, 0);
                    }
                    break;
                default:
                    { // no waiting
                        bytes = recv(socan, &CF, sizeof(struct can_frame),
                                     MSG_DONTWAIT); // recv(*it, buf, 1024, 0);
                    }
                    break;
                }; // switch mode

            if (bytes <= 0)
                {
                    // Если соединение разорвано удаляем сокет
                    close(socan);
                    std::cout << "CAN-SOCKET RCV CLOSED!!!" << std::endl;
                    // continue;
                    // exit(400);
                    return -1;
                }

            F->frm = CF;
            F->decodeId();

            std::cout << "read ok " << bytes << "  frame: " << std::endl;
            return 1;
        }
    else
        {
            std::cout << "time " << timeout_sec << "  is out!" << std::endl;
            return 0;
        }; // READSET

}; // cans::recvFrame()




int cans::sendFrameSub(cant F,uint8_t sub, bool FramePrint){

    int c=KeyCount;

    for(int i=0;i<F.getIdAttrSize();i++){

        if(F.IdAttrKey[i])
           {
                F.setAttrByNdx(i,SubS[sub][c]);
                //printf("send: c=%d, SubS[c]=%d \n",c,SubS[sub][c]);
                c++;
           };

    };//for i

    F.codeId();

    if(FramePrint)
        F.PrintFrame();

    return sendFrame(F);


}; //cans::sendFrameSub(cant F,uint8_t sub,bool FramePrint)

//Проверка принятого фрейма на соответствие подписке
int cans::recvFrameSub(cant *F, int mode, int timeout_sec){

    int REZ=mode;

    //std::cout<<" recvFrameSub"<<std::endl;
    //std::cout<<" REZ="<<REZ<<std::endl;

    if (REZ < 0)
        {
            std::cout << "recvFrameSub ERROR!!!" << std::endl;
            exit(999);
        };

     //std::cout<<" recvFrameSub2"<<std::endl;
    int c=0;
    for(int k=0;k<SubScount;k++){
    bool Q=true;
    for(int i=0;i<F->getIdAttrSize();i++){
      //std::cout<<" recvFrameSub 3"<<std::endl;
        if(F->IdAttrKey[i])
           {

                //std::cout<<" "<<F->getAttrByNdx(i)<<" "<<SubS[k][c]<<"(F->getAttrByNdx(i)==SubS[k][c])"<<(F->getAttrByNdx(i)==SubS[k][c])<<std::endl;
                Q=Q&&(F->getAttrByNdx(i)==SubS[k][c]);
                //std::cout<<" Q="<<Q<<" c="<<c<<std::endl;
                c++;
           };

    };//for i

      if(Q)
         return k;

    };//for k

    return (-1);

};//cans::recvFrameSub(cant *F, int mode, int timeout_sec)

cans::~cans(){

}; // cans::~cans
