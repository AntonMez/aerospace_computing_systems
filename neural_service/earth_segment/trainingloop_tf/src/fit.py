from loguru import logger

from src.warmup import WarmupCosineDecay


def fit(config, model, train, valid):
    total_steps = len(train) * config.n_epoch
    # If not batched
    # total_steps = len(train_set)/config['BATCH_SIZE']*config['EPOCHS']
    # 5% of the steps
    warmup_steps = int(0.05 * total_steps)

    warmup = WarmupCosineDecay(
        model=model,
        total_steps=total_steps,
        warmup_steps=warmup_steps,
        hold=int(warmup_steps / 2),
        start_lr=0.0,
        target_lr=1e-3,
    )

    valloss_list = [float(config.batch_size)]

    for i in range(config.n_epoch):
        logger.debug(f"EPOCH --- {i}/{config.n_epoch} --- ")

        history = model.fit(
            train,
            steps_per_epoch=len(train) / config.batch_size,
            epochs=1,
            validation_data=valid,
            validation_steps=len(valid) / config.batch_size,
            callbacks=warmup,
        )

        valloss_list.append(float(history.history["val_loss"][0]))

        if min(valloss_list[:-1]) > float(history.history["val_loss"][0]):
            logger.debug(f"save model val_loss={valloss_list[-1]} ")
            model.save(f"weights/loss_{valloss_list[-1]}_{config.model_name}.h5")
            print("model save")
            model.save(
                f"weights/loss_{valloss_list[-1]}_{config.model_name}_weights.h5"
            )
            print("weights save")
