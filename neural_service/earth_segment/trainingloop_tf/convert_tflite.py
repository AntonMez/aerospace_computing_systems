import os

import tensorflow as tf

from config.config import config


def convert_model_tf(config):
    weights_list = sorted(os.listdir("weights/"))
    list_loss = []
    for weight in weights_list:
        name_weight = weight.split(".")[1].split("_")[0]
        list_loss.append(float(f"0.{name_weight}"))
        print(float(f"0.{name_weight}"))

    print(list_loss.index(min(list_loss)))
    min_index = list_loss.index(min(list_loss))

    keras_model = tf.keras.models.load_model(
        f"weights/{weights_list[min_index]}", compile=False
    )

    converter = tf.lite.TFLiteConverter.from_keras_model(keras_model)
    tflite_model = converter.convert()

    with open("weights/bestmodel.tflite", "wb") as f:
        f.write(tflite_model)


if __name__ == "__main__":
    convert_model_tf(config)
