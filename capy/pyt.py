from cant import cant

CAN_PROTOCOL_FILENAME = "cantid"

print("Start!")


# print("test:")
# aa=b"321"
# ss=ctypes.c_char_p(b"CABB")
# cant().test(b"bbb")
# cant().test(aa)
# cant().test(ss)

print("getIdAttrSize=", cant().getIdAttrSize())
print("getLoadId=", cant().getLoadId())
print("Step1 !")
print("setAttrByNdx=", cant().setAttrByNdx(1, 4))
print("setAttrByName=", cant().setAttrByName(b"Sender", 2))

print("getAttrByNdx=", cant().getAttrByNdx(3))
print("getAttrByName=", cant().getAttrByName(b"Sender"))
# print("getAttrByName=", cant().getAttrByName(b"Attribute"))

print("Step2 !")
print("setAttrByNdx=", cant().setAttrByNdx(0, 1))
print("setAttrByNdx=", cant().setAttrByNdx(1, 2))
print("setAttrByNdx=", cant().setAttrByNdx(2, 3))
print("setAttrByNdx=", cant().setAttrByNdx(3, 4))

print("Step code !")
print("codeId()      ", cant().codeId())
print("decodeId()    ", cant().decodeId())
print("getAdr()    ", cant().getAdr())

print("Step3 ! Structure Test!")
cant().ClearIdStructure()
print("getIdAttrSize=", cant().getIdAttrSize())
cant().AddIdStruct("Test", 12, True)
cant().InitId()
print("getIdAttrSize=", cant().getIdAttrSize())


print("Step4 !")


# TODO перенести код ниже в библиотеку cant
# >>>
# with open(CAN_PROTOCOL_FILENAME) as protocol_file:
#    lines = protocol_file.readlines()

#   field_names, field_lengths, field_keys = zip(*(line.split() for line in lines))


# cant().SetId(field_names, field_lengths, field_keys)
# <<<

#
print("getIdAttrSize=", cant().getIdAttrSize())

Ns = cant().getIdAttrSize()

for i in range(0, Ns):
    print("i=", i, " MaxVal=", cant().getIdAttrMaxValByNdx(i))
# print(len(StrName))

# for i in range(len(StrName)) :
#   print(StrName[i]," ",StrLen[i])
#    cant().AddIdStruct(StrName[i], StrLen[i])

# cant().InitId()
# print("getIdAttrSize=", cant().getIdAttrSize())


print("Finish!")
