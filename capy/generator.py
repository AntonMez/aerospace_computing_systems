import os
import time

import can
from cantpy import cant

# from cantpy import cant

print("Start!")
print("file=", __file__)
print("CWD=", os.getcwd())
DIRNAME = os.path.dirname(os.path.abspath(__file__))
print("DIR=", DIRNAME)

MY_ID = 11
MY_NET = 0

DATA = b"DEADBEEF"

CAN_PROTOCOL_FILENAME = DIRNAME + "/cantid"

# TODO перенести код ниже в библиотеку cant
# >>>
# with open(CAN_PROTOCOL_FILENAME) as protocol_file:
#   lines = protocol_file.readlines()

# field_names, field_lengths, field_keys = zip(*(line.split() for line in lines))

# TODO почему в pyt.py не используются бинарные строки, а здесь используются?
# field_names = [f_name.encode("ascii") for f_name in field_names]


cantobj = cant()

print("SetID")
cantobj.LoadIdFile(CAN_PROTOCOL_FILENAME)
# cantobj.SetId(field_names, field_lengths, field_keys)

print("setID ok")
# <<<

print("START SET ATTR!")
print("setAttrByName=", cantobj.setAttrByName(b"Sender", MY_ID))
print("setAttrByName=", cantobj.setAttrByName(b"Sender_NET", MY_NET))
print("step 1 Ok")
print("setAttrByName=", cantobj.setAttrByName(b"Reciever", 12))
print("setAttrByName=", cantobj.setAttrByName(b"Reciever_NET", MY_NET))
print("step 2 Ok")
print("setAttrByName=", cantobj.setAttrByName(b"Type", 0))
print("setAttrByName=", cantobj.setAttrByName(b"ID_CRC8", 63))
print("step 3 Ok")
print("codeId()      ", cantobj.codeId())
print("codeId Ok")


print("Set ADR start")

cantobj.setAdr(2067224)
cantobj.decodeId()

for ndx in range(cantobj.getIdAttrSize()):
    print(ndx, ":", cantobj.getAttrByNdx(ndx))

print("SetAdrOk")


bus = can.Bus(interface="socketcan", channel="can0", bitrate=250000)

print("Start Send")

for i in range(10):
    time.sleep(1)
    print("step=", i)
    print("getAdr()    ", cantobj.getAdr())
    DATA = bytes("Step#" + str(i), "ascii")
    # print("msg start")
    msg = can.Message(arbitration_id=cantobj.getAdr(), data=DATA, is_extended_id=True)
    try:
        bus.send(msg)
        # print(f"Message sent on {bus.channel_info}")
    except can.CanError:
        print("Message NOT sent")

bus.shutdown()

print("Finish!")
