# Первичная настройка raspberry для подключения по ssh

1. Установить программу Raspberry Pi Imager.
1. Нажать на кнопку выбора устанавливаемой операционной системы, см.
   @fig:os_selection_button.

    ![Кнопка выбора устанавливаемой операционной
    системы](images/rpi_images_select_os_button.png){#fig:os_selection_button}

1. Выбрать 64-битную версию на основе `Debian Bullseye` без графического интерфейса, см
   @fig:required_os. Если в списке такой нет, то предварительно выбрать `Raspberry Pi OS
   (other)`.

    ![Необходимая версия
    ОС](images/rpi_imager_64_bit_debian_selection.png){#fig:required_os}

1. Перейти в расширенные настройки, см. @fig:rpi_advanced_settings_icon.

    ![Иконка расширенных
    настроек](images/rpi_advanced_settings_icon.png){#fig:rpi_advanced_settings_icon}

1. В расширенных настройках, как показано на @fig:rpi_imager_advanced_options, в целях
   безопасности разрешить аутентификацию только по ssh-ключу, указать логин и пароль
   WiFi-сети (ноутбук и все устройства Raspberry Pi должны находится в одной WiFi-сети),
   указать новый пароль администратора (для дальнейшей установки программ внутри
   Raspberry), указать имя, уникальное для каждой платы Raspberry если планируется
   подключать одновременно несколько устройств в одну сеть.

    ![Расширенные
    настройки](images/rpi_imager_advanced_options.png){#fig:rpi_imager_advanced_options
    height=10cm}

1. Добавить raspberry в файл ~/.ssh/config

    ```config
    Host rpi1.local
        HostName rpi1.local
        User pi
        ForwardAgent yes
    ```

1. В Visual Studio Code открыть меню подключения по ssh, например, нажав `Ctrl+Shift+P`
   и начав писать `ssh connect` будет отображен список похожих команд, выбрать
   `Remote-SSH: Connect to Host...`, см. @fig:vsc_connect_to_ssh_host.

    ![Вызов меню подключения по SSH в
    VSCode](images/vsc_connect_to_ssh_host.png){#fig:vsc_connect_to_ssh_host}

1. Выбрать rpi1.local, см. @fig:vsc_select_configured_ssh_host.

    ![Выбор хоста по
    имени](images/vsc_select_configured_ssh_host.png){#fig:vsc_select_configured_ssh_host}

1. Установить на Raspberry [рекомендуемые для проекта
   расширения](https://stepik.org/lesson/1099950/step/3?unit=1110938) VSCode.
